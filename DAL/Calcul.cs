namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Calcul : DbContext
    {
        public Calcul()
            : base("name=Calcul")
        {
        }

        public virtual DbSet<Statistics> Statistics { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Statistics>()
                .Property(e => e.device_id)
                .IsFixedLength();
        }
    }
}

namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Statistics
    {
        public int id { get; set; }

        [StringLength(10)]
        public string device_id { get; set; }

        public int Calcul { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gateway.Models
{
    public class MetricModel
    {
        public SensorType deviceType { get; set; }
        public string metricValue { get; set; }
        public DateTime metricDate { get; set; }
    }
}
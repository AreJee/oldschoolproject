﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gateway.Models
{
    public enum SensorType
    {
        presenceSensor = 1,
        temperatureSensor = 2,
        brightnessSensor = 3,
        atmosphericPressureSensor = 4,
        humiditySensor = 5,
        soundLevelSensor = 6,
        gpsSensor = 7,
        co2Sensor = 8,
        ledDevice = 9,
        beeperDevice = 10
    };
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gateway.Models;
using Gateway.DeviceEndPoint;
using System.Threading;
using Newtonsoft.Json;


namespace Gateway.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Data Generator";

            return View();
        }
        
        //TODO
        //passer le generateur en tant que classe a part.....

        public ActionResult CreateDevice()
        {
            
            Random rand = new Random();
            List<int> liste = new List<int>();
            List<DeviceModel> deviceList = new List<DeviceModel>();

            int k = rand.Next(1, 10);

            //Generation de device name 
            for (int i = 0; i < k; i++)
            {
                DeviceModel device = new DeviceModel();

                string[] nameGen = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
                int[] test = { rand.Next(0, 25), rand.Next(0, 25), rand.Next(0, 25), rand.Next(0, 25), rand.Next(0, 25), rand.Next(0, 25) };
                device.deviceName = nameGen[0] + nameGen[1] + nameGen[2] + nameGen[3] + nameGen[4] + nameGen[5];

                //Generation Sensor Type 
                Array values = Enum.GetValues(typeof(SensorType));
                device.sensorType = (SensorType)values.GetValue(rand.Next(values.Length));

                deviceList.Add(device);
            }
            Send sender = new Send(deviceList);

            return View();
        }                                         

        //TODO
        //Meme chose qu'en haut

        public ActionResult Generer()
        {
            MetricModel metric;
            Random rand = new Random();
            List<MetricModel> metricList = new List<MetricModel>();

            Array values = Enum.GetValues(typeof(SensorType));

            for (int i = 0; i < 50; i++)
            {
                metric = new MetricModel();
                metric.deviceType = (SensorType)values.GetValue(rand.Next(values.Length));
                metric.metricValue = rand.Next(0, 10000).ToString();
                metric.metricDate = DateTime.Now;

                metricList.Add(metric);
            }

            Send sender = new Send(metricList);

            return View();
        }

        //a refaire
        [HttpGet]
        public ActionResult JsonForm()
        {
            JsonModel json = new JsonModel();
            return View(json);
        }
        //TODO 
        // Generer avec json en entrer
        [HttpPost]
        public ActionResult JsonForm(JsonModel json)
        {
            dynamic o = JsonConvert.DeserializeObject(json.json);
            return View();                                  
        }

        // Generer et envoyer a partir d'un fichier
    }

    class Send
    {
        public DeviceEndPointClient endPoint;

        public Send(List<MetricModel> metricList)
        {
            endPoint = new DeviceEndPointClient();

            endPoint.Open();

            MetricModel[] m = metricList.ToArray();

            var json = JsonConvert.SerializeObject(m);
            endPoint.sendMetric(json);
            
            endPoint.Close();
        }


        public Send(List<DeviceModel> deviceList)
        {
            endPoint = new DeviceEndPointClient();

            endPoint.Open();

            foreach (DeviceModel d in deviceList)
            {
                var json = JsonConvert.SerializeObject(d);
                Properties.Settings.Default["DeviceId"] = endPoint.sendDevice(json);
            }
            endPoint.Close();
        }

        public Send(DeviceModel device)
        {
            //Creation d'un client DeviceEndPoint
            endPoint = new DeviceEndPointClient();
            
            //Ouverture de la connexion 
            endPoint.Open();

            //On convertie en JSON et on envoi 
            var json = JsonConvert.SerializeObject(device);
            Properties.Settings.Default["DeviceId"] = endPoint.sendDevice(json);
            
            //on ferme la connection
            endPoint.Close();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Gateway.Models;
using System.ServiceModel.Web;
using Newtonsoft.Json;
using Gateway.DeviceEndPoint;

namespace Gateway.Controllers
{
    /// <summary>
    /// Point d'entré du device simulator
    /// </summary>
    public class DeviceController : ApiController
    {


        /// <summary>
        /// HTTP : POST
        /// ROUTE : api/device  
        /// Reçois un objet Device en JSON 
        /// et l'envoi au WCF
        /// </summary>
        /// <param name="json" type="string">Objet device</param>
        /// <returns><param name="deviceId" type="string">Id du device</returns>

        [HttpPost]
        [Route("api/device")]
        public string device([FromBody]string json )
        {
            //Creation d'un client DeviceEndPoint
            DeviceEndPointClient endPoint = new DeviceEndPointClient();

            //Ouverture de la connexion 
            endPoint.Open();

            //Envoi des données au WCF
            string id = endPoint.sendDevice(json);

            //Fermeture de la connection
            endPoint.Close();


            return id  ;
        }

        /// <summary>
        /// HTTP : POST
        /// ROUTE : api/deviceList
        /// Reçois plusieurs devices en JSON
        /// les parses et les envois au WCF 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/deviceList")]
        public List<string> deviceList([FromBody]DeviceModel[] json)
        {
            List<DeviceModel> deviceList = new List<DeviceModel>();
            DeviceEndPointClient endPoint = new DeviceEndPointClient();
            List<string> listID = new List<string>();

            deviceList = json.ToList<DeviceModel>();
            endPoint.Open();

            foreach (DeviceModel d in deviceList)
            {
                var sender = JsonConvert.SerializeObject(d);
                listID.Add(endPoint.sendDevice(sender));
            }
            endPoint.Close();

            return listID;
        }


        /// <summary>
        /// HTTP : POST 
        /// ROUTE : api/device/{deviceId}/telemetry/
        /// Récupères les relever des diférents devices et les envois au WCF
        /// </summary>
        /// <param name="deviceId" type="string">parametre a passer dans l'url</param>
        /// <param name="json">Parametre a passer dans le Body du POST</param>
        /// <returns type="bool">validation</returns>
        [HttpPost]
        [Route("api/device/{deviceId}/telemetry/")]
        public bool telemetry(string deviceId, [FromBody]string json)
        {
            //Creation d'un client DeviceEndPoint
            DeviceEndPointClient endPoint = new DeviceEndPointClient();

            //Ouverture de la connexion 
            endPoint.Open();

            //On convertie en JSON et on envoi 
            bool isOk = endPoint.sendMetric(json);

            //on ferme la connection
            endPoint.Close();
            return isOk;
        }
    }
}
                                          
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Contract
{
    [DataContract]
    public enum SensorType
    {
        [EnumMember]
        presenceSensor = 1,
        [EnumMember]
        temperatureSensor = 2,
        [EnumMember]
        brightnessSensor = 3,
        [EnumMember]
        atmosphericPressureSensor = 4,
        [EnumMember]
        humiditySensor = 5,
        [EnumMember]
        soundLevelSensor = 6,
        [EnumMember]
        gpsSensor = 7,
        [EnumMember]
        co2Sensor = 8,
        [EnumMember]
        ledDevice = 9,
        [EnumMember]
        beeperDevice = 10
    };
}
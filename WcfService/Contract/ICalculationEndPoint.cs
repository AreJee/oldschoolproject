﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.Entity;
using DAL;

namespace Contract
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IService1" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    [ServiceKnownType(typeof(SensorType))]
    public interface ICalculationEndPoint
    {

        [OperationContract]
        string GetData(string message);

        [OperationContract]
        string GetDataForCalcul();

        
        [OperationContract]
        [WebGet]
        List<Statistics> getAllCalculatedData();

        [OperationContract]
        void exposeDataContract(SensorType sensorType);
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contract
{                                                                        
    public class Msg
    {
        public string deviceName { get; set; }
        public SensorType sensor { get; set; }
        public DateTime date { get; set; }
        public int numberValue { get; set; }
        public List<int> value { get; set; }
        public string token { get; set; }

        public Msg(string deviceName, SensorType sensor, DateTime date, int numberValue, List<int> value)
        {
            this.deviceName = deviceName;
            this.sensor = sensor;
            this.date = date;
            this.numberValue = numberValue;
            this.value = value;
        }
    }
}
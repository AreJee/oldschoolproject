﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.Entity;
using DAL;

namespace Contract
{
    [ServiceContract]
    [ServiceKnownType(typeof(SensorType))]
    interface IDeviceEndPoint
    {
        [OperationContract]
        bool sendMetric(string o);

        [OperationContract]
        string sendDevice(string o);
    }
}

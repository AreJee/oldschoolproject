﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contract;
using System.Runtime.Serialization;

namespace Service
{
    class MetricModel
    {
        public SensorType deviceType { get; set; }
        public string metricValue { get; set; }
        public DateTime metricDate { get; set; }
    }
}

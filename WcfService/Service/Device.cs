﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contract;

namespace Service
{
    public class Device
    {
        public string name { get; set; }
        public SensorType sensor { get; set; }
        public DateTime acquisitionDate { get; set; }
        public List<int> data { get; set; }
        public int dataNumber { get; set; }
        public bool isConnected { get; set; }

        public Device(Msg message)
        {
            this.name = message.deviceName;
            this.sensor = message.sensor;
            this.acquisitionDate = message.date;
            this.dataNumber = message.numberValue;
            this.data = message.value;
        }                                                                        
        public Device(string message)
        {
            this.name = message;
        }


        private bool Connected()
        {
            bool connect = true;

            return connect;
        }

        private void calibration(Device device)
        {

        }

        private List<int> sendRawData(Device device)
        {

            return device.data;
        }

        private List<int> doCommande(Device device)
        {
            return device.data;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DAL;
using System.Data.Entity;
using Contract;
using System.IO;

namespace Service
{
    class DeviceEndPoint : IDeviceEndPoint
    {
        public bool sendMetric(string o)
        {
            //Déclaration string utiles
            string createText = o + Environment.NewLine;
            string path = @"D:\Projet\Cour\Projet\Developpement\JMSEmulatorMetric.txt";

            //Déclaration pour écriture dans fichier
            FileStream fs = File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            Byte[] info = new UTF8Encoding(true).GetBytes(o);

            try
            {
                //Détermier l'offset pour commencer a écrire dans le ficher
                byte[] bytes = new byte[fs.Length];
                int numBytesToRead = (int)fs.Length;
                int numBytesRead = 0;
                while (numBytesToRead > 0)
                {
                    // Read may return anything from 0 to numBytesToRead.
                    int n = fs.Read(bytes, numBytesRead, numBytesToRead);

                    // Break when the end of the file is reached.
                    if (n == 0)
                        break;

                    numBytesRead += n;
                    numBytesToRead -= n;
                }

                int offset = numBytesRead;
                fs.Write(info, 0, info.Length);
                fs.Close();
                //Partie JMS (Envoi de donnees) a implementer 
                return true;
            }
            catch (Exception)
            {
                fs.Close();
                throw;    
            }
        }

        public string sendDevice(string o)
        {
            string deviceID;
            //Déclaration string utiles
            string createText = o + Environment.NewLine;
            string path = @"D:\Projet\Cour\Projet\Developpement\JMSEmulatorDevice.txt";

            //Déclaration pour écriture dans fichier
            FileStream fs = File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            Byte[] info = new UTF8Encoding(true).GetBytes(o);

            try
            {
                //Détermier l'offset pour commencer a écrire dans le ficher
                byte[] bytes = new byte[fs.Length];
                int numBytesToRead = (int)fs.Length;
                int numBytesRead = 0;
                while (numBytesToRead > 0)
                {
                    // Read may return anything from 0 to numBytesToRead.
                    int n = fs.Read(bytes, numBytesRead, numBytesToRead);

                    // Break when the end of the file is reached.
                    if (n == 0)
                        break;

                    numBytesRead += n;
                    numBytesToRead -= n;
                }

                int offset = numBytesRead;
                fs.Write(info, 0, info.Length);
                fs.Close();
                //Partie JMS (Envoi de donnees) a implementer
                
                return "sdfgjkl";
            }
            catch (Exception)
            {
                fs.Close();
                throw;
            }
        }
    }
}

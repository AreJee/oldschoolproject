﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Contract;
using DAL;
using System.Data.Entity;
using System.IO;

namespace Service
{
    public class CalculationEndPoint : ICalculationEndPoint
    {
        private readonly Calcul Db = new Calcul();

        public string GetData(string message)
        {
            return message;
        }

        public string GetDataForCalcul()
        {
            //Variable lecture fichier (en attendant JMS)
            string path = @"D:\Projet\Cour\Projet\Developpement\JMSEmulatorMetric.txt";
            string json;

            //FileStream fs = File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            json = File.ReadAllText(path);
            //JMS communication ici

            return json;
        }

        public List<Statistics> getAllCalculatedData()
        {
            var model = Db.Statistics.ToList();
            return model;
        }

        public void exposeDataContract(SensorType sensorType)
        {

        }
    }
}
